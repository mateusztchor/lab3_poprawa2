﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

using PK.Test;

namespace Lab3.Test
{
    [TestFixture]
    [TestClass]
    public class P1_Component
    {
        [Test]
        [TestMethod]
        public void P1__I1_Should_Be_An_Interface()
        {
            // Assert
            Assert.That(LabDescriptor.I1.IsInterface);
        }

        [Test]
        [TestMethod]
        public void P1__I2_Should_Be_An_Interface()
        {
            // Assert
            Assert.That(LabDescriptor.I2.IsInterface);
        }

        [Test]
        [TestMethod]
        public void P1__I3_Should_Be_An_Interface()
        {
            // Assert
            Assert.That(LabDescriptor.I3.IsInterface);
        }

        [Test]
        [TestMethod]
        public void P1__I1_Should_Have_Resonable_Number_Of_Methods()
        {
            Helpers.Should_Have_Number_Of_Methods_Between(LabDescriptor.I1, 2, 5);
        }

        [Test]
        [TestMethod]
        public void P1__I2_Should_Have_Resonable_Number_Of_Methods()
        {
            Helpers.Should_Have_Number_Of_Methods_Between(LabDescriptor.I2, 2, 5);
        }

        [Test]
        [TestMethod]
        public void P1__I3_Should_Have_Resonable_Number_Of_Methods()
        {
            Helpers.Should_Have_Number_Of_Methods_Between(LabDescriptor.I3, 2, 5);
        }

        [Test]
        [TestMethod]
        public void P1__Component_Should_Allow_For_Get_Instance_Of_I1()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component);

            // Act
            var iface = LabDescriptor.GetInstanceOfI1(component);

            // Assert
            Assert.That(iface, Is.InstanceOf(LabDescriptor.I1));
        }

        [Test]
        [TestMethod]
        public void P1__Component_Should_Allow_For_Get_Instance_Of_I2()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component);

            // Act
            var iface = LabDescriptor.GetInstanceOfI2(component);

            // Assert
            Assert.That(iface, Is.InstanceOf(LabDescriptor.I2));
        }

        [Test]
        [TestMethod]
        public void P1__Component_Should_Allow_For_Get_Instance_Of_I3()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component);

            // Act
            var iface = LabDescriptor.GetInstanceOfI3(component);

            // Assert
            Assert.That(iface, Is.InstanceOf(LabDescriptor.I3));
        }

        [Test]
        [TestMethod]
        public void P1__Component_Should_Not_Contains_Other_Public_Classes()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component);
            var allowedTypes = new List<Type> {
                LabDescriptor.Component,
                LabDescriptor.I1,
                LabDescriptor.I2,
                LabDescriptor.I3,
                LabDescriptor.Mixin,
                typeof(LabDescriptor.GetInstance),
                typeof(LabDescriptor)
            };
            var types = assembly.GetExportedTypes();

            // Assert
            Assert.That(types, Has.All.Matches<Type>(t => allowedTypes.Contains(t)));
        }
    }
}
